import React from 'react'
import '../css/Homepage.css'

const Homepage = props => {
    return (
        <div className="homepage">
            <p>Olá, neste site mostraremos algumas informações sobre os <b>países</b> do mundo.</p>
            <p>Acesso o menu ao lado para buscar informações dos países.</p>
            <p><b>OBS:</b> Desculpe-nos, o site ainda está em construção.</p>
        </div>
    )
}

export default Homepage